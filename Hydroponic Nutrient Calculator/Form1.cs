﻿using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Xml.Linq;
using System.IO;
//using System.Xml;
//using System.Xml.Serialization;
//using System.Reflection;

namespace Hydroponic_Nutrient_Calculator
{
    public partial class Form1 : Form
    {
        //app version/meta - mainly for build info and About form
        public static string HydroponicNutrientCalculator_Version = "1.0.0";
        public static string HydroponicNutrientCalculator_Type = "BETA";
        public static string HydroponicNutrientCalculator_Releasedate = "01NOV2021"; 
        
        readonly string ProfileFile = Application.StartupPath + "\\profiles.xml";

        public string[,] feedingsarray1 = new string[,]
        {   /* "Aggressive" values from https://generalhydroponics.com/resources/floraseries-feedcharts/  
             * - All default values = milliliters per gallon. Last element is the grow week. */
            {"Cuttings and Seedlings","2.5","2.5","2.5","0.0","Week 1"},
            {"Early Growth One","5.2","4.8","3.7","0.1","Week 2-3"},
            {"Early Growth Two","7.0","6.5","4.8", "0.3","Week 4-5"},
            {"Late Growth / Aggressive","8.5","8.0","6.0","0.1","Week 6"},
            {"Early Bloom","7.6","6.6","8.5","0.1","Flowering - Week 1-2"},
            {"Mid Bloom","6.6","6.6","9.5","0.0","Flowering - Week 3-4"},
            {"Late Bloom","4.7","4.7","5.7","0.0","Flowering - Week 5-6"},
            {"Ripening","2.8","2.8","4.5","0.0","Flowering - Week 7-"},
        };
        public string[,] feedingsarray2 = new string[,]  //  not added yet
        {   /* "Aggressive" values from https://growdoctorguides.com/dwc/dwc-nutrient-calculator/  
             * - All default values = milliliters per gallon. Last element is the grow week. */
            {"Cuttings and Seedlings","1.2","1.2","1.2","0.0","Week 1"},
            {"Mild Vegetative","5.0","5.0","5.0","5.0","Week 2-3"},
            {"Aggressive Vegetative","14.0","10.0","5.0","5.0","Week 3-4"},
            {"Transition to Bloom","10.0","10.0","10.0","0.0","Flowering - Week 1-2"},
            {"Bloom and Ripening","5.0","10.0","14.0","0.0","Flowering - Week 2-7+"},
        };
        public string[,] measurementarray = new string[,]
        {
            {"Milliliters"},
            {"Teaspoons"},
            {"Tablespoons"},
        };
        public string[,] mixarray = new string[,]
        {
            {"Gallon"},
            {"Liter"},
        };

        public Form1()
        {
            InitializeComponent();
            CreateXMLFile();

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // Setup default values / reset view
            NumericUpDown_Unitamount.Value = 1;
            TextBox_Strength_Update.Text = "100";
            for (int i = 0; i < mixarray.GetLength(0); i++)
            {
                ComboBox_Size.Items.Add(mixarray[i, 0]);
            }
            for (int i = 0; i < measurementarray.GetLength(0); i++)
            {
                ComboBox_Measurement.Items.Add(measurementarray[i, 0]);
            }
            for (int i = 0; i < feedingsarray1.GetLength(0); i++)
            {
                ComboBox_Stage.Items.Add(feedingsarray1[i, 0]);
            }
            ComboBox_Size.SelectedItem = (mixarray[0, 0]);  // Gallon
            ComboBox_Measurement.SelectedItem = (measurementarray[0, 0]);  // Milliliters
            ComboBox_Stage.SelectedItem = (feedingsarray1[0, 0]);
//            MessageBox.Show("Start App: ComboBox_Stage.SelectedItem = " + ComboBox_Stage.SelectedItem);
            TextBox_Gro.Text   = Convert.ToDouble(feedingsarray1[0, 1]).ToString("0.00");
            TextBox_Micro.Text = Convert.ToDouble(feedingsarray1[0, 2]).ToString("0.00");
            TextBox_Bloom.Text = Convert.ToDouble(feedingsarray1[0, 3]).ToString("0.00");
            TextBox_Sil.Text =   Convert.ToDouble(feedingsarray1[0, 4]).ToString("0.00");

            Label_Amounts.Text = (string)ComboBox_Measurement.SelectedItem + " per " + (string)ComboBox_Size.SelectedItem;
            LoadProfiles();
            ShowProfileLabel2.Text = loadProfileToolStripMenuItem.DropDownItems[0].Text;
        }
        
        private void ResetForm()
        {
            ComboBox_Size.SelectedItem = (mixarray[0, 0]);  // Gallons
            NumericUpDown_Unitamount.Value = 1;  // Amount to 1 
            ComboBox_Measurement.SelectedItem = (measurementarray[0, 0]);  // Milliliters
            ComboBox_Stage.SelectedItem = (feedingsarray1[0, 0]);  //  "Cuttings and Seedlings"
            TextBox_Strength_Update.Text = "100";
            trackBar_strength.Value = 100;
            Update_Values();
        }

        private void LoadProfiles()
        {
            if (File.Exists(ProfileFile))
            {
                // Load the profiles.xml file
                XDocument xdoc = XDocument.Load(ProfileFile);
                var profiles = xdoc.Descendants("Profile");
                var profileCount = 0;
                foreach (var profile in profiles)
                {  // Needs validation/exception handling here.. works if file is perfect.
                    profileCount++;
                    ToolStripMenuItem item = new ToolStripMenuItem
                    {
                        Name = "profileItem" + profileCount.ToString(),
                        Tag = "Profile",
                        Text = profile.Attribute("Name").Value
                    };
                    item.Click += new EventHandler(ProfileClick);
                    loadProfileToolStripMenuItem.DropDownItems.Add(item);
                }

            }
        }

        public void ProfileClick(object sender, System.EventArgs e)
        {
            //MessageBox.Show(string.Concat("You have Clicked '", sender.ToString(), "' Menu"), "Menu Items Event", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ShowProfileLabel2.Text = sender.ToString();
        }

        private void ComboBox_stage_SelectedIndexChanged(object sender, EventArgs e)
        {
            Update_Values();
        }

        private void NumericUpDown_unitamount_ValueChanged(object sender, EventArgs e)
        {
            Update_Values();
        }

        private void TrackBar_strength_Scroll(object sender, EventArgs e)
        {
            TextBox_Strength_Update.Text = "" + trackBar_strength.Value;
            Update_Values();

        }

        private void ComboBox_Size_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label_Amounts.Text = (string)ComboBox_Measurement.SelectedItem + " per " + (string)ComboBox_Size.SelectedItem;
            Update_Values();
        }

        private void ComboBox_Measurement_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label_Amounts.Text = (string)ComboBox_Measurement.SelectedItem + " per " + (string)ComboBox_Size.SelectedItem;
            Update_Values();
        }

        private void Update_Values()
        {
            //if (ComboBox_Stage.SelectedItem != null)
            //{
            double CurrentStrength = Convert.ToDouble(trackBar_strength.Value) / 100; // make percent a double
           //MessageBox.Show("Update_Values(): ComboBox_Stage.SelectedItem = " + ComboBox_Stage.SelectedItem);
            for (int i = 0; i < feedingsarray1.GetLength(0); i++)
                if (feedingsarray1[i, 0] == (String)ComboBox_Stage.SelectedItem)
                {
                    double gallons_to_liters = 1.0;  // default this to nothing ( * 1 )
                    Label_Week.Text = feedingsarray1[Convert.ToInt32(ComboBox_Stage.SelectedIndex), 5];  // Add the approx grow week description                    
                    if ((String)ComboBox_Size.SelectedItem == mixarray[1, 0]) // Liters?
                    {
                        gallons_to_liters = 0.264172;
                    }
                    if ((String)ComboBox_Measurement.SelectedItem == measurementarray[1, 0]) // Teaspoons?
                    {
                        MainClass myMainClass = new MainClass();
                        TextBox_Gro.Text = myMainClass.Convert_ml_tsp(Convert.ToDouble(feedingsarray1[i, 1]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Micro.Text = myMainClass.Convert_ml_tsp(Convert.ToDouble(feedingsarray1[i, 2]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Bloom.Text = myMainClass.Convert_ml_tsp(Convert.ToDouble(feedingsarray1[i, 3]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Sil.Text = myMainClass.Convert_ml_tsp(Convert.ToDouble(feedingsarray1[i, 4]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                    }
                    else if ((String)ComboBox_Measurement.SelectedItem == measurementarray[2, 0])  // Tablespoons?
                    {
                        MainClass myMainClass = new MainClass();
                        TextBox_Gro.Text = myMainClass.Convert_ml_tbsp(Convert.ToDouble(feedingsarray1[i, 1]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Micro.Text = myMainClass.Convert_ml_tbsp(Convert.ToDouble(feedingsarray1[i, 2]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Bloom.Text = myMainClass.Convert_ml_tbsp(Convert.ToDouble(feedingsarray1[i, 3]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Sil.Text = myMainClass.Convert_ml_tbsp(Convert.ToDouble(feedingsarray1[i, 4]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                    }
                    else // Milliliters
                    {
                        TextBox_Gro.Text = (CurrentStrength * Convert.ToDouble(feedingsarray1[i, 1]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Micro.Text = (CurrentStrength * Convert.ToDouble(feedingsarray1[i, 2]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Bloom.Text = (CurrentStrength * Convert.ToDouble(feedingsarray1[i, 3]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                        TextBox_Sil.Text = (CurrentStrength * Convert.ToDouble(feedingsarray1[i, 4]) * Convert.ToDouble(NumericUpDown_Unitamount.Value) * gallons_to_liters).ToString("0.00");
                    }
                    break;
                }
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(new ProcessStartInfo("https://github.com/bmatthewshea") { UseShellExecute = true }); // Open URL 'about' / support page here..
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void PrintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            {
                string printjob =
                    "Mixing Size           : " + ComboBox_Size.SelectedItem + "\n" +
                    "Mixing Amount         : " + NumericUpDown_Unitamount.Value + "\n" +
                    "Nut. Measurement Size : " + ComboBox_Measurement.SelectedItem + "\n" +
                    "Strength (1-100)      : " + TextBox_Strength_Update.Text + "\n" +
                    "Stage of Growth       : " + ComboBox_Stage.SelectedItem + " (" + feedingsarray1[Convert.ToInt32(ComboBox_Stage.SelectedIndex), 5] + ")\n" +
                    "---------------------- ---------------------------------- \n" +
                    "FloraGro      : " + TextBox_Gro.Text + " " + ComboBox_Measurement.SelectedItem + "\n" +
                    "FloraMicro    : " + TextBox_Micro.Text + " " + ComboBox_Measurement.SelectedItem + "\n" +
                    "FloraBloom    : " + TextBox_Bloom.Text + " " + ComboBox_Measurement.SelectedItem + "\n" +
                    "ProSilica     : " + TextBox_Sil.Text + " " + ComboBox_Measurement.SelectedItem;
                PrintDocument p = new PrintDocument();
                p.PrintPage += delegate (object sender1, PrintPageEventArgs e1)
                {
                    e1.Graphics.DrawString(printjob, new Font("Consolas", 16), new SolidBrush(Color.Black), new RectangleF(0, 0, p.DefaultPageSettings.PrintableArea.Width, p.DefaultPageSettings.PrintableArea.Height));

                };

                PrintDialog printDialog1 = new PrintDialog
                {
                    Document = p
                };
                DialogResult result = printDialog1.ShowDialog();
                if (result == DialogResult.OK)
                    try
                {
                    p.Print();
                }
                catch (Exception ex)
                {
                    throw new Exception("Exception Occured While Printing", ex);
                }
            }
        }

        private void LoadProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //var doc = XDocument.Load(ProfileFile);
        }

        private void SaveProfiletempToolStripMenuItem_Click(object sender, EventArgs e)  // This should only be temp so I can create readble XML profiles for final distribution. (Load only)
        {
            var doc = XDocument.Load(ProfileFile);
            var newElement1 =
              new XElement("Profile",
                  new XAttribute("Name", "General Hydroponics - Flora Series"),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[0, 0]),
                          new XElement("FloraGro", feedingsarray1[0, 1]),
                          new XElement("FloraMicro", feedingsarray1[0, 2]),
                          new XElement("FloraBloom", feedingsarray1[0, 3]),
                          new XElement("ProSilica", feedingsarray1[0, 4]),
                          new XElement("Description", feedingsarray1[0, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[1, 0]),
                          new XElement("FloraGro", feedingsarray1[1, 1]),
                          new XElement("FloraMicro", feedingsarray1[1, 2]),
                          new XElement("FloraBloom", feedingsarray1[1, 3]),
                          new XElement("ProSilica", feedingsarray1[1, 4]),
                          new XElement("Description", feedingsarray1[1, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[2, 0]),
                          new XElement("FloraGro", feedingsarray1[2, 1]),
                          new XElement("FloraMicro", feedingsarray1[2, 2]),
                          new XElement("FloraBloom", feedingsarray1[2, 3]),
                          new XElement("ProSilica", feedingsarray1[2, 4]),
                          new XElement("Description", feedingsarray1[2, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[3, 0]),
                          new XElement("FloraGro", feedingsarray1[3, 1]),
                          new XElement("FloraMicro", feedingsarray1[3, 2]),
                          new XElement("FloraBloom", feedingsarray1[3, 3]),
                          new XElement("ProSilica", feedingsarray1[3, 4]),
                          new XElement("Description", feedingsarray1[3, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[4, 0]),
                          new XElement("FloraGro", feedingsarray1[4, 1]),
                          new XElement("FloraMicro", feedingsarray1[4, 2]),
                          new XElement("FloraBloom", feedingsarray1[4, 3]),
                          new XElement("ProSilica", feedingsarray1[4, 4]),
                          new XElement("Description", feedingsarray1[4, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[5, 0]),
                          new XElement("FloraGro", feedingsarray1[5, 1]),
                          new XElement("FloraMicro", feedingsarray1[5, 2]),
                          new XElement("FloraBloom", feedingsarray1[5, 3]),
                          new XElement("ProSilica", feedingsarray1[5, 4]),
                          new XElement("Description", feedingsarray1[5, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[6, 0]),
                          new XElement("FloraGro", feedingsarray1[6, 1]),
                          new XElement("FloraMicro", feedingsarray1[6, 2]),
                          new XElement("FloraBloom", feedingsarray1[6, 3]),
                          new XElement("ProSilica", feedingsarray1[6, 4]),
                          new XElement("Description", feedingsarray1[6, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray1[7, 0]),
                          new XElement("FloraGro", feedingsarray1[7, 1]),
                          new XElement("FloraMicro", feedingsarray1[7, 2]),
                          new XElement("FloraBloom", feedingsarray1[7, 3]),
                          new XElement("ProSilica", feedingsarray1[7, 4]),
                          new XElement("Description", feedingsarray1[7, 5])
                                      )
                          );
            var newElement2 =
              new XElement("Profile",
                  new XAttribute("Name", "General Hydroponics - GrowDoctor Guides"),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray2[0, 0]),
                          new XElement("FloraGro", feedingsarray2[0, 1]),
                          new XElement("FloraMicro", feedingsarray2[0, 2]),
                          new XElement("FloraBloom", feedingsarray2[0, 3]),
                          new XElement("ProSilica", feedingsarray2[0, 4]),
                          new XElement("Description", feedingsarray2[0, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray2[1, 0]),
                          new XElement("FloraGro", feedingsarray2[1, 1]),
                          new XElement("FloraMicro", feedingsarray2[1, 2]),
                          new XElement("FloraBloom", feedingsarray2[1, 3]),
                          new XElement("ProSilica", feedingsarray2[1, 4]),
                          new XElement("Description", feedingsarray2[1, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray2[2, 0]),
                          new XElement("FloraGro", feedingsarray2[2, 1]),
                          new XElement("FloraMicro", feedingsarray2[2, 2]),
                          new XElement("FloraBloom", feedingsarray2[2, 3]),
                          new XElement("ProSilica", feedingsarray2[2, 4]),
                          new XElement("Description", feedingsarray2[2, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray2[3, 0]),
                          new XElement("FloraGro", feedingsarray2[3, 1]),
                          new XElement("FloraMicro", feedingsarray2[3, 2]),
                          new XElement("FloraBloom", feedingsarray2[3, 3]),
                          new XElement("ProSilica", feedingsarray2[3, 4]),
                          new XElement("Description", feedingsarray2[3, 5])
                          ),
                      new XElement("Stage",
                          new XElement("StageName", feedingsarray2[4, 0]),
                          new XElement("FloraGro", feedingsarray2[4, 1]),
                          new XElement("FloraMicro", feedingsarray2[4, 2]),
                          new XElement("FloraBloom", feedingsarray2[4, 3]),
                          new XElement("ProSilica", feedingsarray2[4, 4]),
                          new XElement("Description", feedingsarray2[4, 5])
                          )
            );
            Console.WriteLine(newElement1);
            Console.WriteLine(newElement2);
            doc.Element("Profiles").Add(newElement1);
            doc.Element("Profiles").Add(newElement2);

            doc.Save(ProfileFile);

            MessageBox.Show("Saved.");
        }
        public void CreateXMLFile()
        {
            if (!File.Exists(ProfileFile))
            {
                //Populate with data here if necessary, then save to make sure it exists
                var xmlFile = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XComment("XML file for storing nutrient profiles"));
                xmlFile.Add(new XElement("Profiles"));
                xmlFile.Save(ProfileFile);
            }
        }



    }
}
