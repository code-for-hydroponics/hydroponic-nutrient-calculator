﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Hydroponic_Nutrient_Calculator
{
    public class MainClass
    {
        public double Convert_ml_tbsp(double ml)
        {
            double convert_ratio_tbsp = 14.787;
            return ml / convert_ratio_tbsp;
        }

        public decimal Convert_ml_tsp(double ml)
        {
            double convert_ratio_ml_tsp = 0.202884;
            return (System.Convert.ToDecimal(ml) * System.Convert.ToDecimal(convert_ratio_ml_tsp));
        }

        public class Profiles
        {
            private readonly List<Profile> _profiles = new List<Profile>();

            [XmlElement("Profile")]
            public List<Profile> ProfileList
            {
                get { return _profiles; }
            }
        }

        public class Profile
        {  // Haven't implemented gets/sets yet - will evolve into this if I work profiles out more.
            [XmlAttribute]
            public string Name { get; set; }

            [XmlElement]
            public string StageName { get; set; }

            [XmlElement]
            public string FloraGro { get; set; }

            [XmlElement]
            public string FloraMicro { get; set; }

            [XmlElement]
            public string FloraBloom { get; set; }

            [XmlElement]
            public string ProSilica { get; set; }

            [XmlElement]
            public string Description { get; set; }
        }

    }
}
