﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hydroponic_Nutrient_Calculator
{
    public class Feedings
    {
        public string Description { get; set; }
        public string FloraGro { get; set; }
        public string FloraMicro { get; set; }
        public string FloraBloom { get; set; }
    }
}
