﻿
namespace Hydroponic_Nutrient_Calculator
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.NumericUpDown_Unitamount = new System.Windows.Forms.NumericUpDown();
            this.trackBar_strength = new System.Windows.Forms.TrackBar();
            this.ComboBox_Stage = new System.Windows.Forms.ComboBox();
            this.TextBox_Gro = new System.Windows.Forms.TextBox();
            this.TextBox_Micro = new System.Windows.Forms.TextBox();
            this.TextBox_Bloom = new System.Windows.Forms.TextBox();
            this.label_litres_gallon = new System.Windows.Forms.Label();
            this.label_units = new System.Windows.Forms.Label();
            this.label_strength = new System.Windows.Forms.Label();
            this.label_stage = new System.Windows.Forms.Label();
            this.label_gro = new System.Windows.Forms.Label();
            this.label_micro = new System.Windows.Forms.Label();
            this.label_bloom = new System.Windows.Forms.Label();
            this.Label_Amounts = new System.Windows.Forms.Label();
            this.TextBox_Strength_Update = new System.Windows.Forms.TextBox();
            this.ComboBox_Size = new System.Windows.Forms.ComboBox();
            this.label_sil = new System.Windows.Forms.Label();
            this.TextBox_Sil = new System.Windows.Forms.TextBox();
            this.ComboBox_Measurement = new System.Windows.Forms.ComboBox();
            this.label_tspml = new System.Windows.Forms.Label();
            this.Label_Week = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProfiletempToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox_About = new System.Windows.Forms.ToolStripTextBox();
            this.ShowProfileLabel1 = new System.Windows.Forms.Label();
            this.ShowProfileLabel2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Unitamount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_strength)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NumericUpDown_Unitamount
            // 
            this.NumericUpDown_Unitamount.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.NumericUpDown_Unitamount.Location = new System.Drawing.Point(176, 83);
            this.NumericUpDown_Unitamount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumericUpDown_Unitamount.Name = "NumericUpDown_Unitamount";
            this.NumericUpDown_Unitamount.ReadOnly = true;
            this.NumericUpDown_Unitamount.Size = new System.Drawing.Size(64, 23);
            this.NumericUpDown_Unitamount.TabIndex = 2;
            this.NumericUpDown_Unitamount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumericUpDown_Unitamount.ValueChanged += new System.EventHandler(this.NumericUpDown_unitamount_ValueChanged);
            // 
            // trackBar_strength
            // 
            this.trackBar_strength.LargeChange = 1;
            this.trackBar_strength.Location = new System.Drawing.Point(153, 129);
            this.trackBar_strength.Maximum = 100;
            this.trackBar_strength.Minimum = 1;
            this.trackBar_strength.Name = "trackBar_strength";
            this.trackBar_strength.Size = new System.Drawing.Size(277, 45);
            this.trackBar_strength.TabIndex = 3;
            this.trackBar_strength.TickFrequency = 5;
            this.trackBar_strength.Value = 100;
            this.trackBar_strength.Scroll += new System.EventHandler(this.TrackBar_strength_Scroll);
            // 
            // ComboBox_Stage
            // 
            this.ComboBox_Stage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Stage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ComboBox_Stage.FormattingEnabled = true;
            this.ComboBox_Stage.Location = new System.Drawing.Point(66, 214);
            this.ComboBox_Stage.Name = "ComboBox_Stage";
            this.ComboBox_Stage.Size = new System.Drawing.Size(230, 23);
            this.ComboBox_Stage.TabIndex = 4;
            this.ComboBox_Stage.SelectedIndexChanged += new System.EventHandler(this.ComboBox_stage_SelectedIndexChanged);
            // 
            // TextBox_Gro
            // 
            this.TextBox_Gro.Location = new System.Drawing.Point(153, 290);
            this.TextBox_Gro.Name = "TextBox_Gro";
            this.TextBox_Gro.ReadOnly = true;
            this.TextBox_Gro.Size = new System.Drawing.Size(143, 23);
            this.TextBox_Gro.TabIndex = 5;
            this.TextBox_Gro.Text = "0";
            // 
            // TextBox_Micro
            // 
            this.TextBox_Micro.Location = new System.Drawing.Point(153, 332);
            this.TextBox_Micro.Name = "TextBox_Micro";
            this.TextBox_Micro.ReadOnly = true;
            this.TextBox_Micro.Size = new System.Drawing.Size(143, 23);
            this.TextBox_Micro.TabIndex = 7;
            this.TextBox_Micro.Text = "0";
            // 
            // TextBox_Bloom
            // 
            this.TextBox_Bloom.Location = new System.Drawing.Point(153, 373);
            this.TextBox_Bloom.Name = "TextBox_Bloom";
            this.TextBox_Bloom.ReadOnly = true;
            this.TextBox_Bloom.Size = new System.Drawing.Size(143, 23);
            this.TextBox_Bloom.TabIndex = 9;
            this.TextBox_Bloom.Text = "0";
            // 
            // label_litres_gallon
            // 
            this.label_litres_gallon.AutoSize = true;
            this.label_litres_gallon.Location = new System.Drawing.Point(20, 65);
            this.label_litres_gallon.Name = "label_litres_gallon";
            this.label_litres_gallon.Size = new System.Drawing.Size(66, 15);
            this.label_litres_gallon.TabIndex = 11;
            this.label_litres_gallon.Text = "Mixing Size";
            // 
            // label_units
            // 
            this.label_units.AutoSize = true;
            this.label_units.Location = new System.Drawing.Point(176, 65);
            this.label_units.Name = "label_units";
            this.label_units.Size = new System.Drawing.Size(90, 15);
            this.label_units.TabIndex = 12;
            this.label_units.Text = "Mixing Amount";
            // 
            // label_strength
            // 
            this.label_strength.AutoSize = true;
            this.label_strength.Location = new System.Drawing.Point(22, 132);
            this.label_strength.Name = "label_strength";
            this.label_strength.Size = new System.Drawing.Size(73, 15);
            this.label_strength.TabIndex = 13;
            this.label_strength.Text = "Strength (%)";
            // 
            // label_stage
            // 
            this.label_stage.AutoSize = true;
            this.label_stage.Location = new System.Drawing.Point(66, 186);
            this.label_stage.Name = "label_stage";
            this.label_stage.Size = new System.Drawing.Size(95, 15);
            this.label_stage.TabIndex = 14;
            this.label_stage.Text = "Stage of Growth:";
            // 
            // label_gro
            // 
            this.label_gro.AutoSize = true;
            this.label_gro.Location = new System.Drawing.Point(46, 293);
            this.label_gro.Name = "label_gro";
            this.label_gro.Size = new System.Drawing.Size(52, 15);
            this.label_gro.TabIndex = 15;
            this.label_gro.Text = "FloraGro";
            // 
            // label_micro
            // 
            this.label_micro.AutoSize = true;
            this.label_micro.Location = new System.Drawing.Point(46, 335);
            this.label_micro.Name = "label_micro";
            this.label_micro.Size = new System.Drawing.Size(64, 15);
            this.label_micro.TabIndex = 16;
            this.label_micro.Text = "FloraMicro";
            // 
            // label_bloom
            // 
            this.label_bloom.AutoSize = true;
            this.label_bloom.Location = new System.Drawing.Point(46, 376);
            this.label_bloom.Name = "label_bloom";
            this.label_bloom.Size = new System.Drawing.Size(68, 15);
            this.label_bloom.TabIndex = 17;
            this.label_bloom.Text = "FloraBloom";
            // 
            // Label_Amounts
            // 
            this.Label_Amounts.AutoSize = true;
            this.Label_Amounts.Location = new System.Drawing.Point(153, 253);
            this.Label_Amounts.Name = "Label_Amounts";
            this.Label_Amounts.Size = new System.Drawing.Size(211, 15);
            this.Label_Amounts.TabIndex = 18;
            this.Label_Amounts.Text = "Measurement per Mixing Size/Amount";
            // 
            // TextBox_Strength_Update
            // 
            this.TextBox_Strength_Update.Location = new System.Drawing.Point(101, 129);
            this.TextBox_Strength_Update.Name = "TextBox_Strength_Update";
            this.TextBox_Strength_Update.ReadOnly = true;
            this.TextBox_Strength_Update.Size = new System.Drawing.Size(42, 23);
            this.TextBox_Strength_Update.TabIndex = 20;
            this.TextBox_Strength_Update.Text = "100";
            // 
            // ComboBox_Size
            // 
            this.ComboBox_Size.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Size.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ComboBox_Size.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ComboBox_Size.FormattingEnabled = true;
            this.ComboBox_Size.Location = new System.Drawing.Point(20, 83);
            this.ComboBox_Size.Name = "ComboBox_Size";
            this.ComboBox_Size.Size = new System.Drawing.Size(106, 23);
            this.ComboBox_Size.TabIndex = 21;
            this.ComboBox_Size.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Size_SelectedIndexChanged);
            // 
            // label_sil
            // 
            this.label_sil.AutoSize = true;
            this.label_sil.Location = new System.Drawing.Point(46, 418);
            this.label_sil.Name = "label_sil";
            this.label_sil.Size = new System.Drawing.Size(62, 15);
            this.label_sil.TabIndex = 24;
            this.label_sil.Text = "ProSilicate";
            // 
            // TextBox_Sil
            // 
            this.TextBox_Sil.Location = new System.Drawing.Point(153, 415);
            this.TextBox_Sil.Name = "TextBox_Sil";
            this.TextBox_Sil.ReadOnly = true;
            this.TextBox_Sil.Size = new System.Drawing.Size(143, 23);
            this.TextBox_Sil.TabIndex = 22;
            this.TextBox_Sil.Text = "0";
            // 
            // ComboBox_Measurement
            // 
            this.ComboBox_Measurement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Measurement.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ComboBox_Measurement.FormattingEnabled = true;
            this.ComboBox_Measurement.Location = new System.Drawing.Point(303, 83);
            this.ComboBox_Measurement.Name = "ComboBox_Measurement";
            this.ComboBox_Measurement.Size = new System.Drawing.Size(127, 23);
            this.ComboBox_Measurement.TabIndex = 27;
            this.ComboBox_Measurement.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Measurement_SelectedIndexChanged);
            // 
            // label_tspml
            // 
            this.label_tspml.AutoSize = true;
            this.label_tspml.Location = new System.Drawing.Point(303, 65);
            this.label_tspml.Name = "label_tspml";
            this.label_tspml.Size = new System.Drawing.Size(127, 15);
            this.label_tspml.TabIndex = 26;
            this.label_tspml.Text = "Nutrient Measurement";
            // 
            // Label_Week
            // 
            this.Label_Week.AutoSize = true;
            this.Label_Week.Location = new System.Drawing.Point(178, 186);
            this.Label_Week.Name = "Label_Week";
            this.Label_Week.Size = new System.Drawing.Size(36, 15);
            this.Label_Week.TabIndex = 28;
            this.Label_Week.Text = "Week";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(451, 24);
            this.menuStrip1.TabIndex = 29;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadProfileToolStripMenuItem,
            this.newToolStripMenuItem,
            this.toolStripSeparator,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // loadProfileToolStripMenuItem
            // 
            this.loadProfileToolStripMenuItem.Name = "loadProfileToolStripMenuItem";
            this.loadProfileToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.loadProfileToolStripMenuItem.Text = "Load Profile";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.newToolStripMenuItem.Text = "&Reload Defaults";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.NewToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(194, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.printToolStripMenuItem.Text = "&Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.PrintToolStripMenuItem_Click);
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem.Image")));
            this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(194, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(141, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(141, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.saveProfiletempToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.customizeToolStripMenuItem.Text = "&Customize";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // saveProfiletempToolStripMenuItem
            // 
            this.saveProfiletempToolStripMenuItem.Name = "saveProfiletempToolStripMenuItem";
            this.saveProfiletempToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.saveProfiletempToolStripMenuItem.Text = "Save Profile (temp)";
            this.saveProfiletempToolStripMenuItem.Click += new System.EventHandler(this.SaveProfiletempToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // ToolStripTextBox_About
            // 
            this.ToolStripTextBox_About.Name = "ToolStripTextBox_About";
            this.ToolStripTextBox_About.Size = new System.Drawing.Size(100, 23);
            // 
            // ShowProfileLabel1
            // 
            this.ShowProfileLabel1.AutoSize = true;
            this.ShowProfileLabel1.Location = new System.Drawing.Point(20, 34);
            this.ShowProfileLabel1.Name = "ShowProfileLabel1";
            this.ShowProfileLabel1.Size = new System.Drawing.Size(102, 15);
            this.ShowProfileLabel1.TabIndex = 30;
            this.ShowProfileLabel1.Text = "PROFILE LOADED:";
            // 
            // ShowProfileLabel2
            // 
            this.ShowProfileLabel2.AutoSize = true;
            this.ShowProfileLabel2.Location = new System.Drawing.Point(128, 34);
            this.ShowProfileLabel2.Name = "ShowProfileLabel2";
            this.ShowProfileLabel2.Size = new System.Drawing.Size(108, 15);
            this.ShowProfileLabel2.TabIndex = 31;
            this.ShowProfileLabel2.Text = "(profile name here)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 481);
            this.Controls.Add(this.ShowProfileLabel2);
            this.Controls.Add(this.ShowProfileLabel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.Label_Week);
            this.Controls.Add(this.ComboBox_Measurement);
            this.Controls.Add(this.label_tspml);
            this.Controls.Add(this.label_sil);
            this.Controls.Add(this.TextBox_Sil);
            this.Controls.Add(this.ComboBox_Size);
            this.Controls.Add(this.TextBox_Strength_Update);
            this.Controls.Add(this.Label_Amounts);
            this.Controls.Add(this.label_bloom);
            this.Controls.Add(this.label_micro);
            this.Controls.Add(this.label_gro);
            this.Controls.Add(this.label_stage);
            this.Controls.Add(this.label_strength);
            this.Controls.Add(this.label_units);
            this.Controls.Add(this.label_litres_gallon);
            this.Controls.Add(this.TextBox_Bloom);
            this.Controls.Add(this.TextBox_Micro);
            this.Controls.Add(this.TextBox_Gro);
            this.Controls.Add(this.ComboBox_Stage);
            this.Controls.Add(this.trackBar_strength);
            this.Controls.Add(this.NumericUpDown_Unitamount);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Hydroponic Nutrient Calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown_Unitamount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_strength)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NumericUpDown NumericUpDown_Unitamount;
        private System.Windows.Forms.TrackBar trackBar_strength;
        private System.Windows.Forms.ComboBox ComboBox_Stage;
        private System.Windows.Forms.TextBox TextBox_Gro;
        private System.Windows.Forms.TextBox TextBox_Micro;
        private System.Windows.Forms.TextBox TextBox_Bloom;
        private System.Windows.Forms.Label label_litres_gallon;
        private System.Windows.Forms.Label label_units;
        private System.Windows.Forms.Label label_strength;
        private System.Windows.Forms.Label label_stage;
        private System.Windows.Forms.Label label_gro;
        private System.Windows.Forms.Label label_micro;
        private System.Windows.Forms.Label label_bloom;
        private System.Windows.Forms.Label Label_Amounts;
        private System.Windows.Forms.TextBox TextBox_Strength_Update;
        private System.Windows.Forms.ComboBox ComboBox_Size;
        private System.Windows.Forms.Label label_sil;
        private System.Windows.Forms.TextBox TextBox_Sil;
        private System.Windows.Forms.ComboBox ComboBox_Measurement;
        private System.Windows.Forms.Label label_tspml;
        private System.Windows.Forms.Label Label_Week;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripTextBox ToolStripTextBox_About;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveProfiletempToolStripMenuItem;
        private System.Windows.Forms.Label ShowProfileLabel1;
        private System.Windows.Forms.Label ShowProfileLabel2;
    }
}

