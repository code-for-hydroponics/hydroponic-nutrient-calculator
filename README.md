# Hydroponic Nutrient Calculator  
#### (General Hydroponics brand nutrients)

---

  - [Installation](#installation)
  - [TO DO](#to-do)
  - [Contact](#contact)
  - [License](#license)

## Installation

- The EXE can be placed anywhere with the `profiles.xml` file.


## Profiles

- The `profiles.xml` holds two profiles currently.

     References for the two default profiles:
     https://generalhydroponics.com/resources/floraseries-feedcharts/ 
     https://growdoctorguides.com/dwc/dwc-nutrient-calculator/ 

- You can create your own profiles by following the XML structure shown in this file. All measurements are in milliliters/ml.
- If you make a mistake adding XML, to reset the file simply erase it.

## TO-DO

- Make a to-do list.

## Contact

- https://discord.gg/MFzuyNs

## License

"Hydroponic Nutrient Calculator" ethminerGUI is released under the [GNU GPLv3](https://opensource.org/licenses/GPL-3.0)